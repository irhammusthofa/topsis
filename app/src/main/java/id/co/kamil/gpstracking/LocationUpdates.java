package id.co.kamil.gpstracking;

import android.app.IntentService;
import android.app.NotificationManager;
import android.content.Intent;
import android.location.Location;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderApi;

/**
 * Created by HP-HP on 26-11-2015.
 */
public class LocationUpdates extends IntentService {
    public static final String ACTION_MyIntentService = "id.co.kamil.gpstracking.RESPONSE";
    public static final String EXTRA_KEY_OUT_LAT = "EXTRA_OUT_LAT";
    public static final String EXTRA_KEY_OUT_LON = "EXTRA_OUT_LON";
    private String TAG = this.getClass().getSimpleName();

    public LocationUpdates() {
        super("GPS Tracker");
    }

    public LocationUpdates(String name) {
        super(name);
    }

    @Override
    protected void onHandleIntent(Intent intent) {

        Log.i(TAG, "onHandleIntent");

        Location location = intent.getParcelableExtra(FusedLocationProviderApi.KEY_LOCATION_CHANGED);
        if(location !=null)
        {
            Log.i(TAG, "onHandleIntent " + location.getLatitude() + "," + location.getLongitude());
            //Toast.makeText(this, "onHandleIntent " + location.getLatitude() + "," + location.getLongitude(), Toast.LENGTH_SHORT).show();
            Intent intentResponse = new Intent();
            intentResponse.setAction(ACTION_MyIntentService);
            intentResponse.addCategory(Intent.CATEGORY_DEFAULT);
            intentResponse.putExtra(EXTRA_KEY_OUT_LAT, location.getLatitude());
            intentResponse.putExtra(EXTRA_KEY_OUT_LON, location.getLongitude());
            sendBroadcast(intentResponse);

            NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
            NotificationCompat.Builder noti = new NotificationCompat.Builder(this);
            noti.setContentTitle("GPS Tracker");
            noti.setContentText(location.getLatitude() + "," + location.getLongitude());
            noti.setSmallIcon(R.mipmap.ic_launcher);
            notificationManager.notify(1234, noti.build());


        }
    }
}
