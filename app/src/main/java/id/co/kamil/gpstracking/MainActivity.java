package id.co.kamil.gpstracking;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.URISyntaxException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import id.co.kamil.gpstracking.utils.Utils;

public class MainActivity extends AppCompatActivity {

    private Button mStartUpdatesButton;
    private Button mStopUpdatesButton;
    private Socket socket;
    private MyBroadcastReceiver myBroadcastReceiver;
    private EditText edtNIK;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        try{
            socket = IO.socket("http://192.168.43.176:1500");
        }catch(URISyntaxException e){
            Toast.makeText(MainActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
            //throw new RuntimeException(e);
        }
        myBroadcastReceiver = new MyBroadcastReceiver();
        mStartUpdatesButton = (Button) findViewById(R.id.start_updates_button);
        mStopUpdatesButton = (Button) findViewById(R.id.stop_updates_button);
        edtNIK = (EditText) findViewById(R.id.nik);

        updateUI();

        mStartUpdatesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!TextUtils.isEmpty(edtNIK.getText())){
                    if (socket.connected()){
                        if(Utils.isMyServiceRunning(MainActivity.this, BackgroundLocationService.class)) {
                            Intent intent = new Intent(MainActivity.this, BackgroundLocationService.class);
                            startService(intent);

                            updateUI();
                        }
                    }else{
                        socket.connect();
                        socket.on("result",handleIncomingMessages);
                        Toast.makeText(MainActivity.this, "Sedang Menghubungkan ke Server", Toast.LENGTH_SHORT).show();
                        new Handler().postDelayed(new Runnable() {

                            @Override
                            public void run() {
                                if(socket.connected()) {

                                    socket.emit("location","OK");
                                    Toast.makeText(MainActivity.this, "Connected", Toast.LENGTH_SHORT).show();
                                    Intent intent = new Intent(MainActivity.this, BackgroundLocationService.class);
                                    startService(intent);

                                    //register BroadcastReceiver
                                    IntentFilter intentFilter = new IntentFilter(LocationUpdates.ACTION_MyIntentService);
                                    intentFilter.addCategory(Intent.CATEGORY_DEFAULT);
                                    registerReceiver(myBroadcastReceiver, intentFilter);


                                    updateUI();
                                }else{
                                    Toast.makeText(MainActivity.this, "Tidak terhubung dengan Server", Toast.LENGTH_SHORT).show();
                                }
                            }
                        }, 2000);

                    }
                }else{
                    Toast.makeText(MainActivity.this, "NIK wajib diisi", Toast.LENGTH_SHORT).show();
                }

            }
        });

        mStopUpdatesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JSONObject sendText = new JSONObject();
                try {
                    sendText.put("id",edtNIK.getText().toString());
                    socket.emit("delete",sendText);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                Intent intent = new Intent(MainActivity.this, BackgroundLocationService.class);
                stopService(intent);

                updateUI();
            }
        });



    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        //un-register BroadcastReceiver
        unregisterReceiver(myBroadcastReceiver);
    }
    private void updateUI() {
        if(Utils.isMyServiceRunning(this, BackgroundLocationService.class)) {
            mStartUpdatesButton.setEnabled(false);
            mStopUpdatesButton.setEnabled(true);
            edtNIK.setEnabled(false);
        } else {
            mStartUpdatesButton.setEnabled(true);
            mStopUpdatesButton.setEnabled(false);
            edtNIK.setEnabled(true);
            if(socket.connected()){
                socket.disconnect();
            }
        }
    }
    private Emitter.Listener handleIncomingMessages = new Emitter.Listener(){
        @Override
        public void call(final Object... args){
            MainActivity.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    JSONObject data = (JSONObject) args[0];
                    String pesan;
                    try {
                        pesan = data.getString("pesan").toString();
                        Toast.makeText(MainActivity.this, pesan, Toast.LENGTH_SHORT).show();
                    } catch (JSONException e) {
                        Toast.makeText(MainActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }
    };
    public class MyBroadcastReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            double lat = intent.getDoubleExtra(LocationUpdates.EXTRA_KEY_OUT_LAT,0);
            double lon = intent.getDoubleExtra(LocationUpdates.EXTRA_KEY_OUT_LON,0);
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date date = new Date();
            Calendar now = Calendar.getInstance();
            now.add(Calendar.SECOND, 10);
            JSONObject sendText = new JSONObject();
            try {
                sendText.put("id",edtNIK.getText().toString());
                sendText.put("lat",lat);
                sendText.put("lon",lon);
                sendText.put("last",now.get(Calendar.YEAR)+"-"+now.get(Calendar.MONTH)+"-"+ now.get(Calendar.DAY_OF_WEEK_IN_MONTH) + " " + now.get(Calendar.HOUR_OF_DAY) + ":"
                        + now.get(Calendar.MINUTE) + ":" + now.get(Calendar.SECOND));
                socket.emit("location",sendText);
            } catch (JSONException e) {
                e.printStackTrace();
            }


            //Toast.makeText(context, lat + "," + lon, Toast.LENGTH_SHORT).show();
        }
    }

}
